# Principles in Refactoring

## Definition: noun and verb

- Noun: A change

Making Code easy to understand and cheaper to modify.

- Verb: Recursive Definition

It's a small step or contains a sequence of small steps.

- quick and composable
- not the same (but from user's point of view, it's the same)
- do not fix bugs when do refactoring

## Two Hats

- Adding functionality
- Refactoring

Only one in the process when coding.

## Why

First, there's no "silver bullet," it can not heal all software ills.

But, it improves the design of software ...

In a word: "It making code easy to understand and cheaper to modify."

## When

### Should

Divided by purpose, there're several types of refactoring:

- Opportunistic
  - Preparatory: when about to add new features
  - Comprehension: when to make the code more readable
  - litter-pickup
- Planned(regular task)
- Long-Term(replacing library or something like that)
- Code Review

### Should Not

- Ugly code that will not be modified
- When rewrite is easier

## Problems

The downside of refactoring:

- Slowing down new features(it's a tradeoff, but generally, we did too little refactoring)
- Code ownership, it's a boundary(May lead things more complicated), the author suggests: "Team owns the code, members do not block modules by default."
- Branch brings difficulty of integration(distinguish merging and integration. (microservices does not have this problem ?)
- Testing, what if making mistakes in refactoring part? => self testing code [link](https://martinfowler.com/bliki/SelfTestingCode.html) => CI => CD
- Legacy code (frequently comes with poor tests)
- Database: is no longer problem area

## Refactoring, architecture, and yagni

1. Old idea: architecture is fixed
2. Refactoring's point of view: you can alter architecture, it improves the design of code (but difficult when there are no decent tests)

Using incremental design or [yagni](https://martinfowler.com/bliki/Yagni.html)(you aren’t going to need it)

Architectural thinking and yagni is a tradeoff.

Refactoring and YAGNI positively reinforce each other.

## Refactoring and the wider software development process

Origin: extreme programming

Three points: continuous integration, self-testing code, refactoring.

They are Composable, latter two combined to TDD.("agile")

- Foundation: self-testing
- Important: CI, to know if the change is to break other's work quickly. self-testing is the critical element of CI.

CD: keeping your software always releasable state

## Refactoring and Performance

Refactoring(making code easier to understand) usually slow down performance.

IMO, there are languages has zero cost abstractions feature, like rust. And furthermore, good design and algorithm are the key points of performance.

Three approaches to do performance programming

- Time budgeting: a real-time system
- Constant attention, using a small story tell us: "measure performance, don’t speculate." PS: profiler is the key, what should we use in nodejs?
- 90% statistic: writing code in a well-factored way(ignore performance), and then focusing on a small part

Summary:

It slows the software in the short term while refactoring, but makes it easier to tune during optimization.

## Where did refactoring come from

It's clear that people rarely write clean code the first time.

IMO small projects(microservices) are more likely to be clean.

Path:

Smalltalk's author => Professor Ralph of Smalltalk community => Ralph's students => Martin

## Automated refactorings

- IDEs (Using `Syntax Tree` is more reliable)
  - eclipse / IntelliJ ...
  - Would be much safer when using static typing languages
- Text editors: search & replace

It depends, can be combined to use.

Remember always using test suite to ensure change.

## Going further

Other practical books...